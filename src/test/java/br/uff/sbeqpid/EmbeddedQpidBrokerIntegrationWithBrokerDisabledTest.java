package br.uff.sbeqpid;


import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
@SpringBootTest(properties="embedded-qpid.enabled=false")
public class EmbeddedQpidBrokerIntegrationWithBrokerDisabledTest {

    @Autowired
    private ApplicationContext applicationContext;

    @Test
    public void verify_if_with_broker_disabled_the_bean_will_not_loaded() {

        Assertions.assertThatThrownBy(()-> applicationContext.getBean(EmbeddedQpidBroker.class))
        .isExactlyInstanceOf(NoSuchBeanDefinitionException.class);
    }


}