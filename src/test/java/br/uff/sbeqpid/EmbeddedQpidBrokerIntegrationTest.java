package br.uff.sbeqpid;


import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
@SpringBootTest()
public class EmbeddedQpidBrokerIntegrationTest {

    @Autowired
    private EmbeddedQpidBroker embeddedQpidBroker;

    @Test
    public void verify_if_context_is_started() {
        //verify
    }


}